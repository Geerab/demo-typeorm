# TypeORM Demo Project

## Introduction

This repository contains application of typeORM demo.

Setup configurations and running instructions can be found below.

## Setup

1. Take the latest version

   ```sh
   git pull
   ```

2. Create `.env` file in the root of repository such that it contains the required environment variables. A sample of this file is provided in repo named `.env.example`.

   - Update `.env` file with suitable configurations

   ```env
   JWT_SECRET=secret
   ```

3. Install node dependencies

   ```sh
   npm install
   ```

4. Build project

   ```sh
   npm run build
   ```

## Running Backend

Run project

   ```sh
   npm run dev
   ```

## API Documentation

https://documenter.getpostman.com/view/5993671/2s7Z12Fj5h

