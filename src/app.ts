import express, { Request, Response, NextFunction } from "express";
import { ValidationError } from "express-validation";

import routes from "./route/index";

const app = express();

app.use(express.json());

app.use("/", routes());

app.use((err: Error, _req: Request, res: Response, _: NextFunction) => {
  if (err instanceof ValidationError) {
    return res.sendStatus(err.statusCode).json(err);
  }
  return res.sendStatus(500).json(err);
});

export { app };

/*
class App {
  public server: Express;

  constructor() {
    this.server = express();

    this.middlewares();
    this.routes();
  }

  middlewares() {
    this.server.use(express.json());
  }

  routes() {
    this.server.use(routes);
    this.server.use((err: Error, _req: Request, res: Response, _: NextFunction) => {
    
      if (err instanceof ValidationError) {
        return res.sendStatus(err.statusCode).json(err);
      }
    
      return res.sendStatus(500).json(err);
    });
  }
}
export default new App().server;

*/
