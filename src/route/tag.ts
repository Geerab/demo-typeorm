import { Router } from "express";
import { validate } from "express-validation";

import TagController, {
  addTagValidation,
  getTagValidation,
  updateTagValidation,
  deleteTagValidation,
} from "../controller/Tag";

const router = Router();

const retrieveTags = (): Router =>
  router.get("/", TagController.retrieveTags);

const createTag = (): Router =>
  router.post("/", TagController.createTag, validate(addTagValidation));

const retrieveTagById = (): Router =>
  router.get(
    "/:id",
    TagController.retrieveTagById,
    validate(getTagValidation)
  );

const updateTag = (): Router =>
  router.put("/:id", TagController.updateTag, validate(updateTagValidation));

const deleteTag = (): Router =>
  router.delete(
    "/:id",
    TagController.deleteTag,
    validate(deleteTagValidation)
  );


export default (): Router => router.use([
    retrieveTags(),
    createTag(),
    retrieveTagById(),
    updateTag(),
    deleteTag()
]);