import { Router } from 'express';

import blogRouter from './blog';
import tagRouter from './tag';
import userRouter from './user';

export default () => {
  const routes = Router();

  routes.use('/blogs', blogRouter());
  routes.use('/tags', tagRouter());
  routes.use('/users', userRouter());

  return routes;
};
