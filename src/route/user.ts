import { Router } from "express";
import { validate } from "express-validation";

import UserController, {
  addUserValidation,
  getUserValidation,
  deleteUserValidation,
} from "../controller/User";

const router = Router();

const retrieveUsers = (): Router =>
  router.get("/", UserController.retrieveUsers);

const createUser = (): Router =>
  router.post("/", UserController.createUser, validate(addUserValidation));

const retrieveUserById = (): Router =>
  router.get(
    "/:id",
    UserController.retrieveUserById,
    validate(getUserValidation)
  );

const deleteUser = (): Router =>
  router.delete(
    "/:id",
    UserController.deleteUser,
    validate(deleteUserValidation)
  );


export default (): Router => router.use([
    retrieveUsers(),
    createUser(),
    retrieveUserById(),
    deleteUser()
]);