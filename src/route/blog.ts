import { Router } from "express";
import { validate } from "express-validation";

import BlogController, {
  addBlogValidation,
  getBlogValidation,
  updateBlogValidation,
  updateBlogPartialValidataion,
  deleteBlogValidation,
} from "../controller/blog";

const router = Router();

const retrieveBlogs = (): Router =>
  router.get("/", BlogController.retrieveBlogs);

const createBlog = (): Router =>
  router.post("/", BlogController.createBlog, validate(addBlogValidation));

const retrieveBlogById = (): Router =>
  router.get(
    "/:id",
    BlogController.retrieveBlogById,
    validate(getBlogValidation)
  );

const updateBlog = (): Router =>
  router.put("/:id", BlogController.updateBlog, validate(updateBlogValidation));

const updateBlogPartial = (): Router =>
  router.patch("/:id", BlogController.updateBlogPartial, validate(updateBlogPartialValidataion));

const deleteBlog = (): Router =>
  router.delete(
    "/:id",
    BlogController.deleteBlog,
    validate(deleteBlogValidation)
  );


export default (): Router => router.use([
    retrieveBlogs(),
    createBlog(),
    retrieveBlogById(),
    updateBlog(),
    updateBlogPartial(),
    deleteBlog()
]);