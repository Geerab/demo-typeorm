import { Request, Response } from "express";
import jwt from "jsonwebtoken";
import { Joi } from 'express-validation';

import { User } from "../model/User";

export const signInValidation = {
  body: Joi.object({ 
    username: Joi.string().max(25).required(), 
    password: Joi.string().max(25).required() 
  }),
};

class Auth {

  async signin(req: Request, res: Response) {
    const { username, password } = req.body;
    if (!(username && password)) {
      res.sendStatus(400);
    }

    let user: User;
    try {
      user = await User.findOneOrFail({ where: { username } });
    } catch (error) {
      return res.sendStatus(401);
    }

    if (!user.checkIfUnencryptedPasswordIsValid(password)) {
      return res.sendStatus(401);
    }

    const token = jwt.sign(
      { userId: user.id, username: user.username },
      process.env.JWT_SECRET as string,
      { expiresIn: "1h" }
    );

    return res.json(token);
  }

}

export default new Auth();
