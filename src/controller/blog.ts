import { Request, Response } from "express";
import { Joi } from 'express-validation';

import { Blog } from "../model/Blog";
import { Tag } from "../model/Tag";
import { User } from "../model/User";

export const addBlogValidation = {
  body: Joi.object({ 
    title: Joi.string().max(25).required(), 
    content: Joi.string().max(500).required(),
    userId: Joi.number().integer().min(1).required(),
  }),
};

export const getBlogValidation = {
  params: Joi.object({ id: Joi.number().integer().min(1).required() }),
};

export const updateBlogValidation = {
  params: Joi.object({ id: Joi.number().integer().min(1).required() }),
  body: Joi.object({ 
    title: Joi.string().max(25).required(), 
    content: Joi.string().max(500).required(),
  }),
};

export const updateBlogPartialValidataion = {
  params: Joi.object({ id: Joi.number().integer().min(1).required() }),
  //body: Joi.object({ userId: Joi.number().integer().min(1) }),
};

export const deleteBlogValidation = {
  params: Joi.object({ id: Joi.number().integer().min(1).required() }),
};

class BlogController {
  async retrieveBlogs(req: Request, res: Response) {
    const blogs = await Blog.find();

    return res.json(blogs);
  }

  async createBlog(req: Request, res: Response) {
    const { title, content, tagId, userId } = req.body;

    const user = await User.findOne({
      where: { id: userId },
    });

    const tag = await Tag.findOne({
      where: { id: tagId },
    });

    const blog = new Blog();

    blog.title = title;
    blog.content = content;
    blog.user = user;
    blog.tag = [tag];

    await blog.save();

    return res.json(blog);
  }

  async retrieveBlogById(req: Request, res: Response) {
    const { id } = req.params;

    const blog = await Blog.findOne({
      where: { id },
      relations: ["user", "tag"],
    });

    return res.json(blog);
  }

  async updateBlog(req: Request, res: Response) {
    const { id } = req.params;
    const { title, content } = req.body;

    const blog = await Blog.findOne(id);

    blog.title = title;
    blog.content = content;

    await blog.save();

    return res.sendStatus(204);
  }

  async updateBlogPartial(req: Request, res: Response) {
    const { id } = req.params;
    const { userId, tagId } = req.body;

    const blog = await Blog.findOne({
      where: { id },
      relations: ["user", "tag"],
    });

    if (userId) {
      const user = await User.findOne({
        where: { id: userId },
      });
      blog.user = user;
    }
    if (tagId) {
      // If tagId is single
      if (typeof tagId === "number") {
        const tag = await Tag.findOne({
          where: { id: tagId },
        });
  
        let mapped = blog.tag.map(ele => ele.id);
        let isExist = mapped.includes(tag.id);
  
        if (isExist === false) {
          blog.tag = [...blog.tag, tag];
        }
      }
      
      if (Array.isArray(tagId)) {
        // TODO: Bulk assign tags into blogs.
      }
    }

    await blog.save();

    return res.sendStatus(204);
  }
 
  async deleteBlog(req: Request, res: Response) {
    const { id } = req.params;

    const blog = await Blog.findOne(id);

    await blog.remove();

    return res.sendStatus(204);
  }
}

export default new BlogController();
