import { Request, Response } from "express";
import { Joi } from 'express-validation';

import { Blog } from "../model/Blog";
import { Tag } from "../model/Tag";
import { User } from "../model/User";

export const addTagValidation = {
  body: Joi.object({ 
    name: Joi.string().max(25).required(), 
    blogId: Joi.number().integer().min(1).required(),
  }),
};

export const getTagValidation = {
  params: Joi.object({ id: Joi.number().integer().min(1).required() }),
};

export const updateTagValidation = {
  params: Joi.object({ id: Joi.number().integer().min(1).required() }),
  body: Joi.object({ 
    name: Joi.string().max(25).required(), 
    blogId: Joi.number().integer().min(1).required(),
  }),
};


export const deleteTagValidation = {
  params: Joi.object({ id: Joi.number().integer().min(1).required() }),
};

class TagController {
  async retrieveTags(req: Request, res: Response) {
    const tags = await Tag.find();

    return res.json(tags);
  }

  async createTag(req: Request, res: Response) {
    const { name, blogId } = req.body;
    
    const blog = await Blog.findOne({
      where: { id: blogId },
    });

    const tag = new Tag();

    tag.name = name;
    tag.blog = [blog];

    await tag.save();

    return res.json(tag);
  }

  async retrieveTagById(req: Request, res: Response) {
    const { id } = req.params;

    const tag = await Tag.findOne({
      where: { id },
      relations: ["blog"],
    });

    return res.json(tag);
  }

  async updateTag(req: Request, res: Response) {
    const { id } = req.params;
    const { name, blogId } = req.body;

    let tag = await Tag.findOne({
      where: { id },
      relations: ["blog"],
    });

    if (name) {
      tag.name = name;
    }

    if (blogId) {
      const blog = await Blog.findOne({
        where: { id: blogId },
      });

      if (tag.blog) {
        let mapped = tag.blog.map(ele => ele.id);
        let isExist = mapped.includes(tag.id);
    
        if (isExist === false) {
          tag.blog = [...tag.blog, blog];
        }

      }
      await tag.save();
    }

    return res.sendStatus(204);
  }

  async deleteTag(req: Request, res: Response) {
    const { id } = req.params;

    const tag = await Tag.findOne(id);

    await tag.remove();

    return res.sendStatus(204);
  }
}

export default new TagController();
