import { Request, Response } from "express";
import { User } from "../model/User";
import { Joi } from 'express-validation';

export const addUserValidation = {
  body: Joi.object({ 
    username: Joi.string().max(25).required(), 
    password: Joi.string().max(25).required() 
  }),
};

export const getUserValidation = {
  params: Joi.object({ id: Joi.number().integer().min(1).required() }),
};

export const deleteUserValidation = {
  params: Joi.object({ id: Joi.number().integer().min(1).required() }),
};

class UserController {

  async retrieveUsers(req: Request, res: Response) {
    const users = await User.find();

    return res.json(users);
  }

  async createUser(req: Request, res: Response) {
    const { username, password } = req.body;

    const user = new User();

    user.username = username;
    user.password = password;
    
    user.hashPassword();

    await user.save();

    return res.json(user);
  }

  async retrieveUserById(req: Request, res: Response) {
    const { id } = req.params;

    const user = await User.findOne({
      where: { id }
    });

    return res.json(user);
  }

  async deleteUser(req: Request, res: Response) {
    const { id } = req.params;

    const user = await User.findOne(id);

    await user.remove();

    return res.sendStatus(204);
  }
}

export default new UserController();
