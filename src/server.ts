import "reflect-metadata";
import { createConnection } from "typeorm";
import * as dotenv from "dotenv";
import { app } from "./app";

dotenv.config();

createConnection({
  type: "postgres",
  host: process.env.DB_HOST,
  port: 5432,
  username: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.DB_NAME,
  synchronize: true,
  logging: false,
  entities: ["src/model/**/*.ts"],
  migrations: ["src/database/migrations/**/*.ts"],
  subscribers: ["src/subscribers/**/*.ts"],
  cli: {
    entitiesDir: "src/model",
    migrationsDir: "src/database/migrations",
    subscribersDir: "src/subscribers",
  },
})
  .then((connection) => {
    app.listen(process.env.APP_PORT);
    console.log("Server started at port: ", process.env.APP_PORT);
  })
  .catch((error) => console.log(error));
